<?php session_start();

var_dump($_SESSION['tasks']);

//Create a TaskList class to hold the methdos in adding, viewing, editing and deleting task

class TaskList{
	public function add($description){
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];
		/*Session variables are set with PHP global variables: $_SESSION*/

		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		//The $newTask will be added in the $_SESSION['tasks'] variable
		array_push($_SESSION['tasks'], $newTask);
	}

	//update a task
	public function update($id, $updatedDescription, $updatedIsFinished){
		$_SESSION['tasks'][$id] ->description = $updatedDescription;
		$_SESSION['tasks'][$id] ->isFinished = ($updatedIsFinished !== null)? true : false;
	}

	//delete a task
	public function delete($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}

	//clear all tasks
	public function clearAll(){
		//removes all data associated with the current session
		session_destroy();
	}
}


//Instantiation of TaskList
//taskList is instatiated from the TaskList() class to have access with its method
$taskList = new TaskList();


if($_POST['action'] === 'add'){
	$taskList -> add($_POST['description']);
}

else if($_POST['action'] === 'update'){
	$taskList -> update($_POST['id'],$_POST['description'],$_POST['isFinished']);
}

else if($_POST['action']=== 'delete'){
	$taskList -> delete($_POST['id']);
}

else if($_POST['action']==='clear'){
	$taskList -> clearAll();
}




//header()
//This is used to send raw HTTP header, but this can also be used to redirect us on specific location
//It will redirect us to the index file upon sending the request.
header("Location: ./index.php");


?>













